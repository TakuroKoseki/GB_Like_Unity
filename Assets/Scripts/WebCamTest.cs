﻿using UnityEngine;

[RequireComponent(typeof(Renderer))]
public class WebCamTest : MonoBehaviour
{
	private Material mat;
	private WebCamTexture webcam;

	private int currentID;

	void Awake()
	{
		mat = GetComponent<Renderer>().material;
		currentID = -1;
	}

	private void SwitchCam()
	{
		CleanUp();

		var cams = WebCamTexture.devices;
		if (cams.Length > 0)
		{
			currentID++;
			currentID %= cams.Length;

			var name = cams[currentID].name;
			webcam = new WebCamTexture(name, 640, 480);
			mat.mainTexture = webcam;
			webcam.Play();

			if (Application.isMobilePlatform)
			{
				transform.rotation = Quaternion.Euler(0f, 0f, 270f);
				var h = 160f;
				var w = h * 4 / 3;
				if (cams[currentID].isFrontFacing)
					w *= -1;
				transform.localScale = new Vector3(w, h, 1f);
			}
		}
	}

	private void CleanUp()
	{
		mat.mainTexture = null;
		if (webcam)
		{
			webcam.Stop();
			Destroy(webcam);
		}
	}

	void Start()
	{
		SwitchCam();
	}

	void Update()
	{
		if (Input.GetMouseButtonDown(0))
			SwitchCam();
	}

	void OnDestroy()
	{
		CleanUp();
	}
}
