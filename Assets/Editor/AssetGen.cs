﻿using UnityEngine;
using UnityEditor;

public static class AssetGen
{
	public const string MENU = "Assets/Create/";

	private const string DIR = "Assets/";
	private const string EXT = ".asset";

	public static void GenerateAsset<T>(
		string name = null, string extension = null, bool focus = true
		) where T : ScriptableObject
	{
		var item = ScriptableObject.CreateInstance<T>();
		Save<T>(item, name, extension);
		if (focus)
			Focus(item);
	}

	private static void Save<T>(Object what, string name, string ext)
	{
		if (string.IsNullOrEmpty(name))
			name = typeof(T).Name;

		if (string.IsNullOrEmpty(ext))
			ext = EXT;

		var path = AssetDatabase.GenerateUniqueAssetPath(DIR + name + ext);

		AssetDatabase.CreateAsset(what, path);
		AssetDatabase.SaveAssets();
	}

	private static void Focus(Object what)
	{
		EditorUtility.FocusProjectWindow();
		Selection.activeObject = what;
	}
}

public static class DitherGen
{
	[MenuItem(AssetGen.MENU + "Dither Setup")]
	public static void GenerateAsset()
	{
		AssetGen.GenerateAsset<DitherSetup>();
	}
}
