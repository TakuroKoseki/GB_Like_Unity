﻿Shader "Custom/Tone Mapping Transparent" {
	Properties {
		_MainTex ("Texture",      2D) = "white" {}
		_Pallet  ("Color Pallet", 2D) = "white" {}
	}
	SubShader {
		Tags {
			"RenderType"  = "Transparent"
			"Queue"       = "Transparent"
			"PreviewType" = "Plane"
		}
		//Blend SrcAlpha OneMinusSrcAlpha
		ZTest Always
		Pass {
CGPROGRAM
#pragma vertex vert
#pragma fragment frag

#include "UnityCG.cginc"

struct appdata
{
	float4 vertex : POSITION;
	float2 uv     : TEXCOORD0;
};

struct v2f
{
	float4 vertex : SV_POSITION;
	float2 uv     : TEXCOORD0;
};

sampler2D _MainTex;
float4 _MainTex_ST;
sampler2D _Pallet;

v2f vert (appdata v)
{
	v2f o;
	o.vertex = mul(UNITY_MATRIX_MVP, v.vertex);
	o.uv = TRANSFORM_TEX(v.uv, _MainTex);
	return o;
}

fixed4 frag (v2f i) : SV_Target
{
	float2 pallet_uv;
	pallet_uv.x = tex2D(_MainTex, i.uv).r;
	pallet_uv.y = 0;
	return tex2D(_Pallet, pallet_uv);
}
ENDCG
		}  // Pass
	}  // SubShader
}  // Shader
