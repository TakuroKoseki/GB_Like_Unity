﻿using UnityEngine;

public class DitherSetup : ScriptableObject
{
	private const string PATTERN = "_Dither";
	private const string SIZE = "_Size";
	private const string WIDTH = "_Width";
	private const string HEIGHT = "_Height";
	private const string RIGHT = "_Right";
	private const int GRAYSCALE = 4;

	public Material ditherMat2;
	public Material ditherMat4;
	public Texture patternTex2;
	public Texture patternTex4;
	public RenderTexture mainTex;

	void Reset()
	{
		SetUp(ditherMat2, patternTex2, 2, 16);
		SetUp(ditherMat4, patternTex4, 4, 64);
	}

	private void SetUp(Material what, Texture ptn, int size, int width)
	{
		var sqr = size * size;
		what.mainTexture = mainTex;
		what.SetTexture(PATTERN, ptn);
		what.SetFloat(SIZE, size);
		what.SetFloat(WIDTH, width);
		what.SetFloat(HEIGHT, sqr);
		what.SetFloat(RIGHT, sqr * (GRAYSCALE - 1) + 1);
	}
}
