﻿Shader "Custom/Dithering" {
	Properties {
		_MainTex ("Texture",                  2D         ) = "white" {}
		_Dither  ("Pattern",                  2D         ) = "gray" {}
		_Size    ("Size",                     Float      ) = 2
		_Right   ("Pattern's Width",          Float      ) = 13
		_Width   ("Pattern Texture's Width",  Float      ) = 16
		_Height  ("Pattern Texture's Height", Float      ) = 4
		_GrayR   ("Grayscale R's Weight",     Range(0, 1)) = 1
		_GrayG   ("Grayscale G's Weight",     Range(0, 1)) = 1
		_GrayB   ("Grayscale B's Weight",     Range(0, 1)) = 1
		_Divisor ("Grayscale's Divisor",      Range(1, 3)) = 3
	}
	SubShader {
		Tags {
			"RenderType"  = "Opaque"
			"PreviewType" = "Plane"
		}
		ZTest Always
		Pass {
CGPROGRAM
#pragma vertex vert
#pragma fragment frag

#include "UnityCG.cginc"

struct appdata
{
	float4 vertex : POSITION;
	float2 uv     : TEXCOORD0;
};

struct v2f
{
	float4 vertex  : SV_POSITION;
	float2 main_uv : TEXCOORD0;
	float4 spos    : TEXCOORD1;
};

sampler2D _MainTex;
float4 _MainTex_ST;
sampler2D _Dither;
float _Size;
float _Width;
float _Height;
float _Right;
float _GrayR;
float _GrayG;
float _GrayB;
float _Divisor;

v2f vert (appdata v)
{
	v2f o;
	o.vertex = mul(UNITY_MATRIX_MVP, v.vertex);
	o.main_uv = TRANSFORM_TEX(v.uv, _MainTex);
	o.spos = ComputeScreenPos(o.vertex);
	return o;
}

fixed4 frag (v2f i) : SV_Target
{
	float2 p = floor(i.spos.xy * _ScreenParams.xy);
	p = fmod(p, _Size);
	fixed4 col = tex2D(_MainTex, i.main_uv);
	float2 uv;
	uv.y = (p.x + p.y * _Size) / _Height;  // Bottom to Top
	uv.x = col.r * _GrayR + col.g * _GrayG + col.b * _GrayB;
	uv.x = floor(uv.x * _Right / _Divisor) / _Width;  // Left to Right
	return tex2D(_Dither, uv);
}
ENDCG
		}  // Pass
	}  // SubShader
}  // Shader
