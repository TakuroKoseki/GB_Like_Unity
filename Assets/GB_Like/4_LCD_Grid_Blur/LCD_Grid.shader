﻿Shader "Custom/LCD Matrix Grid" {
	Properties {
		_MainTex ("Texture",  2D) = "white" {}
		_Grid    ("LCD Grid", 2D) = "gray" {}
	}
	SubShader {
		Tags{
			"Queue"       = "Transparent"
			"RenderType"  = "Transparent"
			"PreviewType" = "Plane"
		}
		Blend SrcAlpha OneMinusSrcAlpha
		Ztest Always
		Pass {
CGPROGRAM
#pragma vertex vert
#pragma fragment frag

#include "UnityCG.cginc"

struct appdata
{
	float4 vertex : POSITION;
	float2 uv     : TEXCOORD0;
};

struct v2f
{
	float4 vertex  : SV_POSITION;
	float2 main_uv : TEXCOORD0;
	float2 grid_uv : TEXCOORD1;
};

sampler2D _MainTex;
float4 _MainTex_ST;
sampler2D _Grid;
float4 _Grid_ST;

v2f vert (appdata v)
{
	v2f o;
	o.vertex = mul(UNITY_MATRIX_MVP, v.vertex);
	o.main_uv = TRANSFORM_TEX(v.uv, _MainTex);
	o.grid_uv = TRANSFORM_TEX(v.uv, _Grid);
	return o;
}

fixed4 frag (v2f i) : SV_Target
{
	fixed4 col = tex2D(_MainTex, i.main_uv);
	col.a *= tex2D(_Grid, i.grid_uv).r;
	return col;
}
ENDCG
		}  // Pass
	}  // SubShader
}  // Shader
