﻿using UnityEngine;
using UnityEngine.UI;

public class DisplayManager : MonoBehaviour
{
	private const int SIZE = 17;
	private const int WIDTH = SIZE * 10;
	private const int HEIGHT = SIZE * 9;
	private const float FACTOR = 20f * 18f / 16f;

	private static DisplayManager instance;

	public static void SetScale(int scale)
	{
		if (!Application.isMobilePlatform)
		{
			if (scale >= 1 && scale <= 8)
				Screen.SetResolution(WIDTH * scale, HEIGHT * scale, false);

			return;
		}

		var res = Screen.currentResolution;
		Screen.SetResolution(res.width / 2, res.height / 2, true);
		//instance.str = "W: " + res.width + "  H: " + res.height;

		var size = FACTOR * res.height / res.width;
		foreach (var c in instance.outputCams)
			c.orthographicSize = size;
	}

	public int defaultScale;
	public Camera[] outputCams;

	/*
	public Text debugOutput;
	private string str;
	private int count = 0;
	*/

	void Reset()
	{
		defaultScale = 4;
	}

	void Awake()
	{
		instance = this;
		SetScale(defaultScale);
	}

	/*
	void Update()
	{
		count++;
		if (count == 2)
		{
			var res = Screen.currentResolution;
			str += " -> W: " + res.width + "  H: " + res.height;
			instance.debugOutput.text = str;
		}
	}
	*/
}
