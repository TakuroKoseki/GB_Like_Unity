﻿using UnityEngine;

[RequireComponent(typeof(Camera))]
public class Feedback : MonoBehaviour
{
	public RenderTexture copyFrom;
	public RenderTexture copyTo;

	void Reset()
	{
		copyFrom = GetComponent<Camera>().targetTexture;
	}

	void OnPostRender()
	{
		Graphics.Blit(copyFrom, copyTo);
	}
}
