﻿using UnityEngine;

[RequireComponent(typeof(Camera))]
public class Clearner : MonoBehaviour
{
	private static Clearner instance;

	public static void Clear()
	{
		instance.gameObject.SetActive(true);
	}

	public RenderTexture clearTarget;

	void Reset()
	{
		clearTarget = GetComponent<Camera>().targetTexture;
	}

	void Awake()
	{
		instance = this;
	}

	void OnPostRender()
	{
		gameObject.SetActive(false);
	}
}
